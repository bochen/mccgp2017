import ml.dmlc.xgboost4j.scala.Booster
import ml.dmlc.xgboost4j.scala.spark.XGBoost
import ml.dmlc.xgboost4j.scala.spark.XGBoostModel

import org.apache.spark.sql.functions.{col,udf}
import org.apache.spark.sql.functions.avg
import org.apache.spark.sql.Dataset
import org.apache.spark.ml.linalg.{DenseVector,Vectors,Vector}
import org.apache.spark.sql.functions.{col,udf}
import org.apache.spark.sql.functions.avg
import org.apache.spark.sql.Dataset

import org.apache.spark.ml.linalg.{DenseVector,Vectors,Vector}
import org.apache.spark.sql.functions._
    
val filename="/tmp/m/xgb_X_d11.parquet"
val X=spark.read.parquet(filename).filter($"label">=0).cache()
    
    
val mse_elemwise = udf((pred:Float,label:Float ) => math.pow(pred-label,2))

  
def make_train(df:Dataset[_], sample:Boolean=true)={

    val _train=
        if(sample) 
            df.sample(false,0.0001).cache()
        else
            df

    val train=_train
    //train
    train.randomSplit(Array(0.8,0.2))

}

def process_one( )={
    println(s"Start")
     val Array(train,test)=make_train(df=X,sample=false)

    val paramMap = List(
      "eta" -> 0.3f,
      "max_depth" -> 6,
       // "min_child_weight" -> 50,
        //   "base_score" -> 0,
       //"gamma" -> 0,
      "obj_type"  -> "reg:linear",
        "subsample" -> 1, 
        "colsample_bytree" -> 1,         
        "objective" -> "reg:linear"        
    ).toMap
    val numRound = 100
    
    val xgboostModel = XGBoost.trainWithDataFrame(
    train, paramMap, numRound, nWorkers=150, useExternalMemory = true, featureCol="features", labelCol="label")    

    val mae_elemwise = udf((pred:Float,label:Float ) => math.abs(pred-label))
    val smape_elemwise= udf((pred:Float,label:Float ) =>{
        if (pred==label && label == 0)
            0
        else
            2*math.abs(pred-label)/(math.abs(pred)+math.abs(label))
    })

  if(true){ 
    val pred_trans=udf((v:  Float ) => math.exp(v))
    //val pred_trans=udf((v:  Float ) => v)
    var result1=xgboostModel.transform(train.sample(false,0.3)).withColumn("prediction2", pred_trans(col("prediction"))).cache()
    println(s"MAD, SMAPE on label2:  ${evaluate1(result1)}")
    println(s"MAD, SMAPE on label:  ${evaluate2(result1)}")
    result1=xgboostModel.transform(test).withColumn("prediction2", pred_trans(col("prediction"))).cache()
    println(s"MAD, SMAPE on label2:  ${evaluate1(result1)}")
    println(s"MAD, SMAPE on label:  ${evaluate2(result1)}")
        
        
    result1.unpersist()        
   }

    val modelfilename=s"/tmp/wxgb/model2_log_mix_1.xgb"
    xgboostModel.write.overwrite.save(modelfilename)

    train.unpersist()    
    println(s"End D\n")
  
}

process_one

System.exit(0)
    
